﻿using System;

namespace Yash_AbstExtraExam_Aug11
{
    abstract public class vehicle
    {
        public String name = "cars";

        public abstract void show();
    }

    public class car : vehicle
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class cl
    {
        public String name = "book";

        public abstract void show();
    }

    public class students : cl
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class theatre
    {
        public String name = "allmovie";

        public abstract void show();
    }

    public class allmovie : theatre
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class playstore
    {
        public String name = "apps";

        public abstract void show();
    }

    public class apps : playstore
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class library
    {
        public String name = "book";

        public abstract void show();
    }

    public class book : library
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            book b = new book(); b.show();
            car v = new car(); v.show();
            allmovie m = new allmovie(); m.show();
            apps a = new apps(); a.show();
            students st = new students(); st.show();

        }
    }
}
