﻿using System;

namespace Yash_Array_Aug10
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] mon = { "Monday" };
            string[] tue = { "Tuesday" };
            string[] wed = { "Wednesday" };

            string[][] day = { mon, tue, wed };

            foreach (string[] i in day)
            {
                Console.WriteLine(i[0]);
            }

        }

    }
}
